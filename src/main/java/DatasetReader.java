import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class DatasetReader {

    public static List<RFile> loadDataSet(String fileName) throws FileNotFoundException {
        JsonParser jsonParser = new JsonParser();
        JsonElement jsonElement = jsonParser.parse(new FileReader(new File(fileName)));
        JsonArray jsonArray = (JsonArray)jsonElement;
        List<RFile> rFiles = new ArrayList<>();
        jsonArray.forEach(jsonElement1 -> {
            JsonObject jsonObject = (JsonObject)jsonElement1;
            File rfile = new File("files/"+jsonObject.get("file_name").getAsString());
            byte[] hash = new byte[0];
            try {
                hash = Hex.decodeHex(jsonObject.get("hash").getAsString());
            } catch (DecoderException e) {
                e.printStackTrace();
            }
            rFiles.add(new RFile(rfile,hash));
        });
        return rFiles;

    }

}
