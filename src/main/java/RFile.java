import org.apache.commons.codec.binary.Hex;

import java.io.File;
import java.util.Arrays;

public class RFile {
    private File file;
    private byte[] hash;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public RFile(File file, byte[] hash) {
        this.file = file;
        this.hash = hash;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return String.format("[file '%s'(%d bytes)::hash '%s']",file.getName(),file.length(), Hex.encodeHexString(hash));
    }
}
