import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.Streams;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.codec.binary.Hex;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Random;

public class RandomFileGenerator {
    private Random random;
    public static final String ALPHA_NUMERIC = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final int FILE_NAME_LEN = 10;
    public static final int BUFLEN = 10240;


    public RandomFileGenerator(){
        random = new SecureRandom();

    }

    public RFile  generateNew(long size) throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("SHA1");
        byte[] buffer = new byte[BUFLEN];
        String fileName = getNewFileName();
        File fileDirectory = new File(Main.DIRECTORY);

        if(!fileDirectory.exists()){
            fileDirectory.mkdir();
        }
        boolean exists = fileDirectory.exists();
        boolean isdirectory = fileDirectory.isDirectory();
        String absolute = fileDirectory.getAbsolutePath();
        File file = new File(Main.DIRECTORY+fileName);
        if(!file.exists())file.createNewFile();

        OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));

        long done = 0;
        while(done < size){
            int nextRead = (int)Math.min((long)BUFLEN,size-done);
            random.nextBytes(buffer);
            outputStream.write(buffer,0,nextRead);
            digest.update(buffer, 0, nextRead);
            done = done + nextRead;

        }
        outputStream.close();
        byte[] hash = digest.digest();
        return new RFile(file,hash);

    }

    public String getNewFileName(){
        StringBuilder stringBuilder = new StringBuilder();
        for(int i=0;i<FILE_NAME_LEN;i++){
            int randint = random.nextInt(ALPHA_NUMERIC.length());
            stringBuilder.append(ALPHA_NUMERIC.charAt(randint));
        }
        return stringBuilder.toString();
    }

    public static void write(File file, List<RFile> rfiles) throws IOException {
        if(!file.exists())file.createNewFile();
        JsonArray jsonArray = new JsonArray();
        rfiles.stream().forEach(rFile -> {
            JsonObject jsonObject = new JsonObject();
            File file1 = rFile.getFile();
            String fileName = file1.getName();
            byte[] hash = rFile.getHash();
            String hashhex = Hex.encodeHexString(hash);
            jsonObject.addProperty("file_name",fileName);
            jsonObject.addProperty("hash",hashhex);
            jsonArray.add(jsonObject);

        });
        JsonWriter jsonWriter = new JsonWriter(new FileWriter(file));
        jsonWriter.setIndent(" ");
        Streams.write(jsonArray,jsonWriter);
        jsonWriter.close();
    }


}
