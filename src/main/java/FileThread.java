import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileThread implements Runnable{
    public static final String HOST = "127.0.0.1";
    public static final int PORT = 5053;

    private final List<RFile> filesToSend = new ArrayList<>();

    public void addRFile(RFile rFile){
        filesToSend.add(rFile);
    }


    @Override
    public void run() {
        for(RFile rFile:filesToSend){
            FileConnection fileConnection = new FileConnection();
            try {
                fileConnection.connect(HOST, PORT);
                String status = fileConnection.sendFile(rFile.getFile(),rFile.getHash());
                Main.fileString.put(rFile.getFile().getName(),rFile);
                Main.statusString.put(rFile.getFile().getName(),status);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
