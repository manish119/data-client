
import org.apache.commons.codec.binary.Hex;

import java.io.*;
import java.net.*;

public class FileConnection {
    private Socket socket;
    public static  final long boundary = 8292902600220228L;
    public static final int MAX_BUFFER = 10240;

    public void connect(String address, int port) throws IOException {
        socket = new Socket();
        socket.connect(new InetSocketAddress(InetAddress.getByName(address),port),20000);
    }

    public Socket getSocket() {
        return socket;
    }

    public String sendFile(File file, byte[] hash) throws IOException {
        String fileName = file.getName();
        long length = file.length();
        OutputStream outputStream = socket.getOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.writeLong(boundary);
        dataOutputStream.writeLong(length);
        dataOutputStream.writeUTF(fileName);
        outputStream.write(hash);
        String hashHex = Hex.encodeHexString(hash);
        dataOutputStream.writeLong(boundary);
        InputStream inputStreamFile = new BufferedInputStream(new FileInputStream(file));
        InputStream inputStreamSocket = socket.getInputStream();
        long written = 0;
        byte[] buffer = new byte[MAX_BUFFER];
        DataInputStream dataInputStreamSocket = new DataInputStream(inputStreamSocket);
        while (written<length){
            if(inputStreamSocket.available()>0){

                String response = dataInputStreamSocket.readUTF();
                System.out.println(response);
                inputStreamFile.close();
                return response;
            }
            long toread = Math.min(length-written,(long)MAX_BUFFER);
            inputStreamFile.read(buffer,0, (int)toread);
            outputStream.write(buffer,0,(int)toread);
            written = written + toread;
           // System.out.print(String.format("file '%s' hash '%s':: %d of %d bytes sent",fileName,hashHex,written, length));


        }
        inputStreamFile.close();
        while(inputStreamSocket.available()==0)
            Thread.yield();
        String writtens = dataInputStreamSocket.readUTF();
       // System.out.println(String.format("file '%s' hash '%s'::  Status '%s'",fileName,hashHex,written, length, writtens));
        return writtens;



    }

}
