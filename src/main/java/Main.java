import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    public static final String DIRECTORY = "files/";
    public static final String JSON_DIR = "filedata";
    public static final String JSON_FILE = "data.json";

    public static final Map<String,String> statusString = new ConcurrentHashMap<>();
    public static final Map<String,RFile> fileString = new ConcurrentHashMap<>();

    public static void main(String[] args){
        try {
            if(args.length < 1){
                System.out.println("Insufficient arguments");
                return;
            }
            if("filegen".equals(args[0])){
                if(args.length<1){
                    System.out.println("Insufficient arguments");
                    return;
                }
                long filesize = Long.parseLong(args[1]);
                int num = Integer.parseInt(args[2]);
                List<RFile> rFiles = generateRandomFiles(filesize,num);
                File datadirectory = new File("filedata");
                if(!datadirectory.exists())datadirectory.mkdir();
                RandomFileGenerator.write(new File(JSON_DIR+"/"+JSON_FILE),rFiles);
            }else if("client".equals(args[0])){
                Date date = new Date();
                long start = date.getTime();
                int concurrency=1;
                if(args.length>1){
                    concurrency = Integer.parseInt(args[1]);
                }

                List<RFile> allFiles = DatasetReader.loadDataSet(JSON_DIR+"/"+JSON_FILE);

                List<FileThread> threads = divideFiles(concurrency,allFiles);
                List<Thread> allthreads = new ArrayList<>(threads.size());
                for(FileThread fileThread: threads){
                    Thread thread = new Thread(fileThread);
                    allthreads.add(thread);
                    thread.start();
                }
                for(Thread thread:allthreads){
                    thread.join();
                }
                for(String fname: statusString.keySet()){
                    System.out.println(fileString.get(fname)+"::Status "+statusString.get(fname));
                }
                Date end = new Date();
                long fin = end.getTime();
                System.out.println(String.format("Total time %d milis",fin-start));


            }

        }catch(Exception exp){
            exp.printStackTrace();
        }

    }

    public static List<RFile> generateRandomFiles(long filesize, int num) throws IOException, NoSuchAlgorithmException {
        RandomFileGenerator randomFileGenerator = new RandomFileGenerator();
        List<RFile> rFileList = new ArrayList<>(num);
        for(int ind=0;ind<num;ind++){
            RFile file = randomFileGenerator.generateNew(filesize);
            rFileList.add(file);
        }
        return rFileList;
    }

    public static List<FileThread> divideFiles(int concurrency, List<RFile> rFiles){
        List<FileThread> fileThreads = new ArrayList<>(concurrency);
        for(int i=0; i<concurrency; i++){
            fileThreads.add(new FileThread());
        }
        for(int i=0;i<rFiles.size();i++){
            fileThreads.get(i%concurrency).addRFile(rFiles.get(i));
        }
        return fileThreads;
    }
}
